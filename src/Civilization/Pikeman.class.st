Class {
	#name : 'Pikeman',
	#superclass : 'Unit',
	#category : 'Civilization',
	#package : 'Civilization'
}

{ #category : 'as yet unclassified' }
Pikeman >> Ap [ 
^5* 1 + (0.5 * (self level -1 ))
]

{ #category : 'as yet unclassified' }
Pikeman >> Atm: aUnit [
^1

]

{ #category : 'as yet unclassified' }
Pikeman >> Dp [ 
^20* 1+ (0.75 * (self level -1)) 
]

{ #category : 'as yet unclassified' }
Pikeman >> Dtm: aUnit [
^ aUnit attackAgainstPikeman: self
]

{ #category : 'as yet unclassified' }
Pikeman >> attack: aUnit [

^ aUnit attackAgainstPikeman: self
]

{ #category : 'as yet unclassified' }
Pikeman >> attackAgainstArcher: anArcher [
|Dtm|
^ Dtm:=1.
]

{ #category : 'as yet unclassified' }
Pikeman >> attackAgainstKnight: aKnight [
|Dtm|
^ Dtm:=2.
]

{ #category : 'as yet unclassified' }
Pikeman >> attackAgainstPikeman: aPikeman [
|Dtm|
^ Dtm:=1.
]

{ #category : 'as yet unclassified' }
Pikeman >> attackAgainstWarrior: aWarrior [
|Dtm|
^ Dtm:=1.
]
