Class {
	#name : 'Knight',
	#superclass : 'Unit',
	#category : 'Civilization',
	#package : 'Civilization'
}

{ #category : 'as yet unclassified' }
Knight >> Ap [ 
^20* 1 + (0.5 * (self level -1 ))
]

{ #category : 'as yet unclassified' }
Knight >> Atm: aUnit [
^ aUnit attackAgainstKnight: self

]

{ #category : 'as yet unclassified' }
Knight >> Dp [ 
^5* 1+ (0.75 * (self level -1)) 
]

{ #category : 'as yet unclassified' }
Knight >> Dtm: aUnit [
^ 1
]

{ #category : 'as yet unclassified' }
Knight >> attack: aUnit [

^ aUnit attackAgainstKnight: self
]

{ #category : 'as yet unclassified' }
Knight >> attackAgainstArcher: anArcher [
|Atm|
^ Atm:=2.

]

{ #category : 'as yet unclassified' }
Knight >> attackAgainstKnight: aKnight [
|Atm|
^ Atm:=2.
]

{ #category : 'as yet unclassified' }
Knight >> attackAgainstPikeman: aPikeman [
^1
]

{ #category : 'as yet unclassified' }
Knight >> attackAgainstWarrior: aWarrior [
|Atm|
^ Atm:=2.
]
