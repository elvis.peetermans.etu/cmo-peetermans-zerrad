Class {
	#name : 'Tile',
	#superclass : 'Object',
	#instVars : [
		'unit',
		'type'
	],
	#category : 'Civilization',
	#package : 'Civilization'
}

{ #category : 'as yet unclassified' }
Tile >> Ati [
^0
]

{ #category : 'initialization' }
Tile >> initialize [ 
unit := false.
type := 0.
]

{ #category : 'accessing' }
Tile >> type [
^type
]

{ #category : 'accessing' }
Tile >> type: aType [
type := aType
]

{ #category : 'as yet unclassified' }
Tile >> typeFlatDti: aType [
^0
]

{ #category : 'as yet unclassified' }
Tile >> typeHillyDti: aType [
^ 10
]

{ #category : 'accessing' }
Tile >> unit [ 
^unit
]

{ #category : 'accessing' }
Tile >> unit: aState [

unit:= aState
]
