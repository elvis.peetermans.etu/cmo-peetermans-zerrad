Class {
	#name : 'Grid',
	#superclass : 'Object',
	#instVars : [
		'tile'
	],
	#category : 'Civilization',
	#package : 'Civilization'
}

{ #category : 'initialization' }
Grid >> initialize [ 

tile := 0
]

{ #category : 'accessing' }
Grid >> tile [
^tile
]

{ #category : 'accessing' }
Grid >> tile: aType [

tile := aType
]
