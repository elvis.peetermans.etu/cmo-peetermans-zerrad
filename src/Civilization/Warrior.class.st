Class {
	#name : 'Warrior',
	#superclass : 'Unit',
	#category : 'Civilization',
	#package : 'Civilization'
}

{ #category : 'as yet unclassified' }
Warrior >> Ap [ 
^10* 1 + (0.5 * (self level -1 ))
]

{ #category : 'as yet unclassified' }
Warrior >> Atm: aUnit [
^1
]

{ #category : 'as yet unclassified' }
Warrior >> Dp [ 
^10* 1+ (0.75 * (self level -1)) 
]

{ #category : 'as yet unclassified' }
Warrior >> Dtm: aUnit [
^1
]

{ #category : 'as yet unclassified' }
Warrior >> attack: aUnit [

^ aUnit attackAgainstWarrior: self
]

{ #category : 'as yet unclassified' }
Warrior >> attackAgainstArcher: anArcher [
]

{ #category : 'as yet unclassified' }
Warrior >> attackAgainstKnight: aKnight [
]

{ #category : 'as yet unclassified' }
Warrior >> attackAgainstPikeman: aPikeman [
]

{ #category : 'as yet unclassified' }
Warrior >> attackAgainstWarrior: aWarrior [
]
