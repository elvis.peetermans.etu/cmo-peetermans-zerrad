Class {
	#name : 'Archer',
	#superclass : 'Unit',
	#category : 'Civilization',
	#package : 'Civilization'
}

{ #category : 'as yet unclassified' }
Archer >> Ap [
^20 * 1 + (0.5 * (self level -1 ))
]

{ #category : 'as yet unclassified' }
Archer >> Atm: aUnit [
^ 1
]

{ #category : 'as yet unclassified' }
Archer >> Dp [
^5* 1+ (0.75 * (self level -1)) 
]

{ #category : 'as yet unclassified' }
Archer >> Dtm: aUnit [
^1
]

{ #category : 'as yet unclassified' }
Archer >> attack: aUnit [

^ aUnit attackAgainstArcher: self
]

{ #category : 'as yet unclassified' }
Archer >> attackAgainstArcher: anArcher [
]

{ #category : 'as yet unclassified' }
Archer >> attackAgainstKnight: aKnight [
]

{ #category : 'as yet unclassified' }
Archer >> attackAgainstPikeman: aPikeman [
]

{ #category : 'as yet unclassified' }
Archer >> attackAgainstWarrior: aWarrior [
]
