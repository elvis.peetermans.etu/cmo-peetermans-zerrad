Class {
	#name : 'Unit',
	#superclass : 'Object',
	#instVars : [
		'type',
		'health',
		'level'
	],
	#category : 'Civilization',
	#package : 'Civilization'
}

{ #category : 'testing' }
Unit class >> isAbstract [

	^ self == Unit
]

{ #category : 'as yet unclassified' }
Unit >> Ap [
^self subclassResponsibility 
]

{ #category : 'as yet unclassified' }
Unit >> Atm: aUnit [
^self subclassResponsibility 
]

{ #category : 'as yet unclassified' }
Unit >> Dp [ 
^self subclassResponsibility 
]

{ #category : 'as yet unclassified' }
Unit >> Dtm: aUnit [
^self subclassResponsibility 
]

{ #category : 'as yet unclassified' }
Unit >> attack: aUnit [

self subclassResponsibility 
]

{ #category : 'as yet unclassified' }
Unit >> attackAgainstArcher: anArcher [

self subclassResponsibility 
]

{ #category : 'as yet unclassified' }
Unit >> attackAgainstKnight: aKnight [

self subclassResponsibility 
]

{ #category : 'as yet unclassified' }
Unit >> attackAgainstPikeman: aPikeman [

self subclassResponsibility 
]

{ #category : 'as yet unclassified' }
Unit >> attackAgainstWarrior: aWarrior [

self subclassResponsibility 
]

{ #category : 'as yet unclassified' }
Unit >> attackingUnitDamage: aTile [
|Ad|
^ Ad := self Dp * self Dtm + aTile Ati 
]

{ #category : 'as yet unclassified' }
Unit >> defendingUnitDamage: aUnit [
|Dd Dti Atm|
^ Dd := self Ap * self Atm: aUnit
]

{ #category : 'accessing' }
Unit >> health [ 
^health
]

{ #category : 'initialization' }
Unit >> initialize [ 
type := 0.
health := 100.
level := 1
]

{ #category : 'accessing' }
Unit >> level [ 
^level 
]

{ #category : 'accessing' }
Unit >> type [
^type
]
