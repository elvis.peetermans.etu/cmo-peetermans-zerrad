Class {
	#name : 'TestCivilisation',
	#superclass : 'TestCase',
	#category : 'Civilization',
	#package : 'Civilization'
}

{ #category : 'tests' }
TestCivilisation >> testK2W3H [
self assert: ((Knight attack: Warrior new) type: #hilly ) equals:#x
]

{ #category : 'tests' }
TestCivilisation >> testW1W1F [
self assert: (((Warrior level: 1) attack: (Warrior level: 1) new) type: #hilly ) equals:#x
]
